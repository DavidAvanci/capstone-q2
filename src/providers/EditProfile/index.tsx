import { createContext, ReactNode, useContext, useState } from "react";
import { api } from "../../services/api";

interface IEditProfileProviderProps {
  children: ReactNode;
}

interface IDadosDoUsuario {
  name: string;
  email: string;
  password: string;
  age: number;
  weight: number;
  height: number;
  gender: string;
  objective: string;
  activity: string;
  type: string;
}

interface IEditProfileData {
  editarDadoPerfil: (patch: string, valorDoInput: string) => void;
  PegarDadosDoUsuario: () => void;
  dadosDoUsuario: IDadosDoUsuario;
  dados: any;
}

const EditProfileContext = createContext<IEditProfileData>(
  {} as IEditProfileData
);

export const EditProfileProvider = ({
  children,
}: IEditProfileProviderProps) => {
  const [dados, setDados] = useState(false);
  const [dadosDoUsuario, setDadosDoUsuario] = useState<IDadosDoUsuario>(
    {} as IDadosDoUsuario
  );

  const PegarDadosDoUsuario = () => {
    api
      .get(
        `https://api-fake-nutriself.herokuapp.com/users/${localStorage.getItem(
          "dadosDoUsuario"
        )}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("accesstoken")}`,
          },
        }
      )
      .then((response) => {
        setDadosDoUsuario(response.data);
      })
      .then((_) => setDados(true))
      .catch((error) => console.log(error));
  };

  const editarDadoPerfil = (patch: string, valorDoInput: string) => {
    const valor = {};
    Object.defineProperty(valor, patch, {
      enumerable: true,
      configurable: true,
      writable: true,
      value: valorDoInput,
    });
    api
      .patch(
        `https://api-fake-nutriself.herokuapp.com/users/${localStorage.getItem(
          "dadosDoUsuario"
        )}`,
        valor,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("accesstoken")}`,
          },
        }
      )
      .catch((error) => console.log(error));
  };

  return (
    <EditProfileContext.Provider
      value={{
        PegarDadosDoUsuario,
        dadosDoUsuario,
        editarDadoPerfil,
        dados,
      }}
    >
      {children}
    </EditProfileContext.Provider>
  );
};

export const useEditProfileContext = () => useContext(EditProfileContext);
