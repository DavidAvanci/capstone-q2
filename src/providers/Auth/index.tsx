import { createContext, ReactNode, useContext, useState } from "react";
import { useHistory } from "react-router";
import { IUserData } from "../../types";
import { ILoginData } from "../../types";
import jwt_decode from "jwt-decode";
import { api } from "../../services/api";
import toast from "react-hot-toast";
const AuthContext = createContext<IAuthProviderData>({} as IAuthProviderData);
interface IUserProps {
  children: ReactNode;
}
interface IAuthProviderData {
  auth: string;
  token: string;
  registerUser: (data: IUserData) => void;
  signIn: (data: ILoginData) => void;
  signOut: () => void;
  isLoggedIn: boolean;
  accesToken: string;
}
export const AuthProvider = ({ children }: IUserProps) => {
  const token = localStorage.getItem("token") || "";
  const [auth, setAuth] = useState<string>(JSON.stringify(token));
  const [accesToken, setAccesToken] = useState<string>(JSON.stringify(token));
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(token ? true : false);
  const history = useHistory();
  const registerUser = async (data: IUserData) => {
    await api
      .post(`/register`, data)
      .then((response) => console.log(response))
      .catch((error) => console.log(error));
  };
  const signIn = async (data: ILoginData) => {
    await api
      .post(`/login`, data)
      .then((response) => {
        setAccesToken(response.data.accessToken);
        localStorage.setItem(
          "token",
          JSON.stringify(response.data.accessToken)
        );
        localStorage.setItem("accesstoken", response.data.accessToken);
        const decodedToken = jwt_decode<any>(response.data.accessToken);
        localStorage.setItem("dadosDoUsuario", decodedToken.sub);
        setAuth(decodedToken);
        setIsLoggedIn(true);
        history.push("/profile");
        toast.success("Bem-vindo(a) de volta!");
      })
      .catch((error) => toast.error("Ops! Algo errado ocorreu!"));
      };

  const signOut = () => {
    localStorage.removeItem("token");
    setIsLoggedIn(false);
  };
  return (
    <AuthContext.Provider
      value={{
        auth,
        token,
        registerUser,
        signIn,
        signOut,
        isLoggedIn,
        accesToken,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
export const useAuth = () => useContext(AuthContext);
