import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { apiFoods } from "../../services/api";

interface IFoodsData {
  foodsList: IFoodList[];
  categoryList: ICategoryList[];
}
interface ICategoryList {
  id: number;
  category: string;
}

interface IFoodsProviderProps {
  children: ReactNode;
}

interface IAtributes {
  energy: ISeila;
  protein: ISeila;
  lipid: ISeila;
  carbohydrate: ISeila;
  cholesterol: ISeila;
  potassium: ISeila;
  sodium: ISeila;
  fiber: ISeila;
  fatty_acids: ISeila;
}

interface ISeila {
  qty: number;
  unit: number;
  kcal: number;
  kj: number;
  saturated: IMacro;
  monounsaturated: IMacro;
  polyunsaturated: IMacro;
}

interface IMacro {
  qty: number;
  unit: number;
}

interface IFoodList {
  id: number;
  description: string;
  base_qty: number;
  base_unit: string;
  category_id: number;
  attributes: IAtributes;
}

const FoodsContext = createContext<IFoodsData>({} as IFoodsData);

export const FoodsProvider = ({ children }: IFoodsProviderProps) => {
  const [foodsList, setFoodsList] = useState<IFoodList[]>([]);
  const [categoryList, setCategoryList] = useState<ICategoryList[]>([]);
  useEffect(() => {
    apiFoods.get("").then((res) => setFoodsList(res.data));
  }, []);

  return (
    <FoodsContext.Provider value={{ foodsList, categoryList }}>
      {children}
    </FoodsContext.Provider>
  );
};

export const useFoodsContext = () => useContext(FoodsContext);
