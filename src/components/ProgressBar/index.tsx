import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { Container, GlobalContainer } from "./styles";

interface IProgressBar {
  score: number;
  maxValue: number;
}

const CircularProgessBarComp = ({ score, maxValue }: IProgressBar) => {
  return (
    <GlobalContainer>
      <Container>
        <CircularProgressbarWithChildren
          value={score}
          maxValue={maxValue}
          circleRatio={1}
          styles={buildStyles({
            rotation: 0,
            strokeLinecap: "butt",
            textSize: "16px",
            pathTransitionDuration: 0.5,
            // Colors
            pathColor: `${
              (score / maxValue) * 100 <= 100
                ? "var(--color-primary)"
                : "var(--color-terciary)"
            }`,
            textColor: "#000",
            trailColor: "#d6d6d6",
          })}
          strokeWidth={10}
        >
          <div style={{ fontSize: 18, marginTop: 0, marginBottom: 10 }}>
            <strong>{`${maxValue} kcal`}</strong>
          </div>
          <div style={{ fontSize: 15, marginTop: -5, color: "gray" }}>
            <strong>{`${Math.round((score / maxValue) * 100)} %`}</strong>
          </div>
        </CircularProgressbarWithChildren>
      </Container>
    </GlobalContainer>
  );
};

export default CircularProgessBarComp;
