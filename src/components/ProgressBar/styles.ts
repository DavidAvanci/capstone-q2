// style={{
//     width: 125,
//     height: 125,
//     margin: "0 auto",
//     marginTop: "18px",
//     marginBottom: "18px",
//   }}

import styled from "styled-components";

export const Container = styled.div`
  width: 125px;
  height: 125px;
  margin: 0 auto;
  margin-top: 18px;
  margin-bottom: 18px;
`;

export const GlobalContainer = styled.div`
  max-width: 50vh;
  width: 400px;
  height: 125px;
  margin: 0 auto;
  margin-top: 18px;
  margin-bottom: 18px;
`;
