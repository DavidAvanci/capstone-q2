import styled from "styled-components";

export const Container = styled.div`
  div {
  }
  div.label--input {
    padding-left: 16px;
    color: var(--color-placeholder);
    text-align: left;
  }

  span {
    color: var(--color-error);
    padding-left: 16px;
    text-align: left;
    display: block;
  }
`;

export const InputContainer = styled.div`
  border: 1px solid var(--color-primary);
  border-radius: 16px 4px 16px 4px;

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-content: center;
  align-items: center;
  padding: 0px 8px;
  height: 48px;
  /* margin-bottom: 32px; */

  .iconContainer {
    padding: 0px 8px;
    height: 18px;
    color: var(--color-placeholder);
  }

  input {
    border: none;
    height: 32px;
    outline: none;
    border-radius: 16px 4px 16px 4px;
    font-size: 16px;
    flex: 1;
    border: 0;
    color: var(--color-placeholder);
    padding-left: 8px;
  }
  input::placeholder {
    color: var(--color-placeholder);
    font-size: 16px;
  }

  svg {
    margin-right: 1;
  }
`;
