import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import Button from "../Button";
import { useAuth } from "../../providers/Auth";
import { IUserData } from "../../types";
import { useHistory } from "react-router-dom";
import TextField from "../TextField";
import { AiFillLock, AiFillMail } from "react-icons/ai";
const FormLogin = () => {
  const { signIn } = useAuth();
  const schema = yup.object().shape({
    email: yup
      .string()
      .email("Formato inválido")
      .required("Campo obrigatório!"),
    password: yup
      .string()
      .required("Campo obrigatório!")
      .min(6, "Mínimo de 6 caracteres"),
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IUserData>({
    resolver: yupResolver(schema),
  });
  const onSubmit = async (data: IUserData) => {
    await signIn(data);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField
        icon={AiFillMail}
        label="Teu e-mail"
        type="text"
        name="email"
        register={register}
        placeholder="Ex.: andre@nutriself.com"
        error={errors.email?.message}
      />
      <TextField
        icon={AiFillLock}
        type="password"
        label="Tua senha"
        name="password"
        register={register}
        placeholder="Ex.: 123456"
        error={errors.password?.message}
      />
      <Button type="submit">Entrar</Button>
    </form>
  );
};
export default FormLogin;
