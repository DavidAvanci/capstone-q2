import { Carousel } from "@spoqa/react-carousel";
import "@spoqa/react-carousel/carousel.css";
import { CarouselContainer } from "./style";
import crsl1 from "../../assets/crsl1.png";
import crsl2 from "../../assets/crsl2.png";
import crsl3 from "../../assets/crsl3.png";
import { useCarouselContext } from "../../providers/Carousel";

export const CarouselComponent = () => {
  const { changeImgIdx } = useCarouselContext();

  return (
    <CarouselContainer>
      <Carousel onPositionChange={(e) => changeImgIdx(e)}>
        <div>
          <img src={crsl1} alt="img1" />
          <h2>Coma de maneira saudavel</h2>
          <p>Manter uma boa saúde é um excelente objetivo para a vida.</p>
        </div>
        <div>
          <img src={crsl2} alt="img2" />
          <h2>Equilibrio alimentar</h2>
          <p>Saiba quais os nutrientes que te completam</p>
        </div>
        <div>
          <img src={crsl3} alt="img3" />
          <h2>Ajuda de nutricionistas</h2>
          <p>Profissionais qualificados te guiarão no processo à boa vida.</p>
        </div>
      </Carousel>
    </CarouselContainer>
  );
};
