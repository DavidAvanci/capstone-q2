import styled from "styled-components";

interface progressBar {
  progress: number;
  foodGrans: number;
  totalGrans: number;
}

export const ProgressBar = styled.div<progressBar>`
  margin-bottom: 18px;
  width: 400px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  align-content: center;
  margin: 0 auto;

  .border {
    border-radius: 16px;
    width: 120px;
    height: fit-content;
    background-color: var(--color-terciary-two);
  }
  .graph {
    background-color: ${(props) =>
      (props.foodGrans / props.totalGrans) * 100 <= 100
        ? "var(--color-terciary)"
        : "red"};
    height: 10px;
    width: ${(props) => `${props.progress}%`};
    border-radius: 16px;
  }

  .macroContainer {
    width: 124px;
  }

  .detailContainer {
    width: 85px;
  }
  .macros {
    font-size: 20px;
    margin-right: 20px;
    font-weight: bolder;
  }

  .graphDetail {
    font-size: 13px;
    margin-left: 20px;
    font-size: 24px;
    color: var(--color-primary);
    font-weight: bolder;
    margin-left: 0 auto;
  }
`;
