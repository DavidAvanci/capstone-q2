import { ProgressBar } from "./styles";

interface IProgressBar {
  foodGrans: number;
  totalGrans: number;
  macro: string;
}

const MiniBarDestop = ({ foodGrans, macro, totalGrans }: IProgressBar) => {
  return (
    <ProgressBar
      progress={
        (foodGrans / totalGrans) * 100 >= 100
          ? 100
          : (foodGrans / totalGrans) * 100
      }
      foodGrans={foodGrans}
      totalGrans={totalGrans}
    >
      <div className="macroContainer">
        <span className="macros">{macro}</span>
      </div>
      <div className="border">
        <div className="graph"></div>
      </div>
      <div className="detailContainer">
        <span className="graphDetail">
          {((foodGrans / totalGrans) * 100).toFixed(2)}%
        </span>
      </div>
    </ProgressBar>
  );
};
export default MiniBarDestop;
