import { ProgressBar } from "./styles";

interface IProgressBar {
  foodGrans: number;
  totalGrans: number;
  macro: string;
}

const MiniProgessBar = ({ foodGrans, macro, totalGrans }: IProgressBar) => {
  return (
    <ProgressBar
      progress={
        (foodGrans / totalGrans) * 100 >= 100
          ? 100
          : (foodGrans / totalGrans) * 100
      }
      foodGrans={foodGrans}
      totalGrans={totalGrans}
    >
      <span className="graphDetail">{`${foodGrans}/${totalGrans}`}</span>
      <div className="border">
        <div className="graph"></div>
      </div>
      <span className="macros">{macro}</span>
    </ProgressBar>
  );
};
export default MiniProgessBar;
