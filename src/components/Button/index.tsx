import { ReactNode } from "react";
import { ButtonStyle } from "./styles";

interface IButton extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  type?: "button" | "submit" | "reset" | undefined;
}

const Button = ({ children, type, ...rest }: IButton) => {
  return <ButtonStyle {...rest}>{children}</ButtonStyle>;
};

export default Button;
