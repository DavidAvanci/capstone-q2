import styled from "styled-components";

export const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 80px;
  border: 0px 0px 1px 0px solid rgba(0, 0, 0, 0.4);
  padding: 10px;
  box-shadow: -1px 10px 5px -6px rgba(0, 0, 0, 0.4);
  position: sticky;
  top: 0;
  background-color: var(--color-base-default);
  z-index: 1;

  img {
    width: 61px;
    height: 61px;
    color: var(--color-primary);
  }

  .imgContainer {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-left: 16px;

    span {
      padding: 10px;
      font-weight: bolder;
      font-size: 16px;
    }
  }

  .menu {
    margin-left: auto;
    
  }

  svg.menu--open {
    display: block;
    margin-right: 16px;
    @media (min-width: 880px){
      display: none;
    }
  }

  svg.menu--close {
    display: block;
    margin-right: 16px;
    @media (min-width: 880px){
      display: none;
    }
  }
`;


export const ButtonFlexContainer = styled.div`

  display: none;

  @media (min-width: 880px) {

    display: flex;
    width: 700px;
  }
`;


export const MenuButton = styled.button`
  width: 100px;
  height: 32px;
  font-size: 16px;
  font-family: "Signika", sans-serif;
  text-align: center;
  cursor: pointer;
  color: var(--color-base-default);
  background-color: transparent;
  border: 2px solid transparent;
  :hover {
    background-color: var(--color-base-default);
    border: 2px solid var(--color-base-default);
    color: var(--color-primary);
    border-radius: 16px;
  }
  :active {
    background-color: var(--color-terciary);
    border: 2px solid var(--color-terciary-two);
    color: var(--color-terciary-two);
    border-radius: 16px;
  }

  @media (min-width: 880px) {
    color: var(--color-primary);
    width: 140px;
    font-size: 20px;
    :hover {
    background-color: var(--color-primary);
    border: 2px solid var(--color-primary);
    color: var(--color-base-default);
    border-radius: 16px;
  }
  :active {
    background-color: var(--color-terciary);
    border: 2px solid var(--color-terciary-two);
    color: var(--color-terciary-two);
    border-radius: 16px;
  }
  }

  @media (min-width: 1100px){
    width: 240px;
    height: 40px;
    font-size: 20px;

  }


`;

export const Menuhamburguer = styled.ul`
  
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  position: absolute;

  height: 400px;
  top: 40px;
  right: 40px;
  opacity: 1;
  background: var(--color-primary);
  justify-content: space-evenly;
  z-index: 3;
  border: 2px solid var(--color-base-default);
  /* transition: all 2s ease; */

  li {
    z-index: 4;
    background: var(--color-primary);
    justify-content: flex-end;
    align-items: center;
    list-style: none;
    padding: 0 8px;
  }
`;
