import NutriselfLogo from "../../assets/nutriselfUserLogo.png";
import { CgMenuGridR } from "react-icons/cg";
import { useEditProfileContext } from "../../providers/EditProfile";
import { useEffect, useState } from "react";
import {
  ButtonFlexContainer,
  HeaderContainer,
  MenuButton,
  Menuhamburguer,
} from "./styles";
import { useHistory } from "react-router-dom";
import toast from "react-hot-toast";
export const Header = () => {
  const { PegarDadosDoUsuario, dadosDoUsuario } = useEditProfileContext();

  useEffect(() => {
    PegarDadosDoUsuario();
  }, []);

  const history = useHistory();
  const [menuDrop, setMenuDrop] = useState<boolean>(false);

  const toSend = (path: string) => {
    return history.push(path);
  };

  const OpenMenu = () => {
    setMenuDrop(true);
  };

  const CloseMenu = () => {
    setMenuDrop(false);
  };

  const handleLogout = () => {
    localStorage.clear();
    history.push("/");
  };

  return (
    <HeaderContainer>
      <div className="imgContainer">
        <img src={NutriselfLogo} alt="logo"></img>
        <span>Olá, {dadosDoUsuario && dadosDoUsuario.name}!</span>
      </div>
      {menuDrop ? (
        <CgMenuGridR
          size={30}
          color="a52451"
          className="menu--close"
          onClick={CloseMenu}
        />
      ) : (
        <CgMenuGridR
          size={30}
          color="91c787"
          className="menu--open"
          onClick={OpenMenu}
        />
      )}
      {menuDrop ? (
        <Menuhamburguer>
          <li>
            <MenuButton onClick={() => toSend("/profile")}>Perfil</MenuButton>
          </li>
          <li>
            <MenuButton onClick={() => toSend("/Search")}>Pesquisar</MenuButton>
          </li>
          <li>
            <MenuButton onClick={() => toSend("/dietPlan/choose")}>
              Dieta
            </MenuButton>
          </li>
          <li>
            <MenuButton onClick={() => toSend("/about")}>Sobre</MenuButton>
          </li>
          <li>
            <MenuButton onClick={() => toast.error("Em construção!")}>
              Mensagens
            </MenuButton>
          </li>

          <li>
            <MenuButton onClick={handleLogout}>Sair</MenuButton>
          </li>
        </Menuhamburguer>
      ) : (
        <></>
      )}

      <ButtonFlexContainer>
        <MenuButton onClick={() => toSend("/profile")}>Perfil</MenuButton>
        <MenuButton onClick={() => toSend("/Search")}>Pesquisar</MenuButton>
        <MenuButton onClick={() => toSend("/dietPlan/choose")}>
          Dieta
        </MenuButton>
        <MenuButton onClick={() => toSend("/About")}>Sobre</MenuButton>
        <MenuButton onClick={() => toast.error("Em construção!")}>
          Mensagens
        </MenuButton>
        <MenuButton onClick={handleLogout}>Sair</MenuButton>
      </ButtonFlexContainer>
    </HeaderContainer>
  );
};
