import React, { useEffect, useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import AddButton from "@material-ui/icons/AddCircle";
import { useFoodsContext } from "../../providers/Foods";
import NoFood from "../../assets/noFood.png";
import { CardMedia } from "@material-ui/core";
import { useHistory } from "react-router";
import { useUserDietContext } from "../../providers/UserDiet";
import { ContainerCards } from "./styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "90%",
      maxWidth: 345,
      minHeight: 70,
      border: "1px solid black",
      marginBottom: "15px",
      margin: "0 auto",
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    caloriesMeal: {
      color: "rgba(108, 182, 99, 1)",
      fontWeight: "bold",
      fontSize: "18px",
    },
    header: {
      fontSize: "24px",
      fontWeight: "bolder",
      padding: "5px 10px",
    },
    bottonHeader: {
      padding: "0px",
    },
    cardMedia: {
      width: "80px",
      height: "80px",
      margin: "0 auto",
    },
    imgContainer: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    cardMediaSpan: {
      color: "rgba(0, 0, 0, 0.5)",
    },
  })
);

interface IMeal {
  name: string;
  foodsId: number[];
  id?: any;
}

interface IDietCard {
  meal: IMeal;
  mealId?: number;
}

const DietCard = ({ meal, mealId }: IDietCard) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const { foodsList } = useFoodsContext();
  const [buttonDelet, setButtonDelet] = useState<boolean>(false);
  const [IdButton, setIdButton] = useState<number>();
  const { setMealId, removeFoodFromDiet } = useUserDietContext();
  const { getDiet, newFoodsIds } = useUserDietContext();

  useEffect(() => {
    getDiet();
  }, [newFoodsIds]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const history = useHistory();

  const remover = (id: number, mealId: number, idx: number) => {
    removeFoodFromDiet(id, mealId, idx);
    setMealId(mealId);
  };
  const CallButton = (number: number) => {
    setIdButton(number);
    setButtonDelet(!buttonDelet);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        className={classes.header}
        action={
          <IconButton
            aria-label="settings"
            onClick={() => {
              setMealId(mealId);
              history.push(`/addFood/${mealId}`);
            }}
          >
            <AddButton />
          </IconButton>
        }
        title={meal.name}
      />
      <CardActions disableSpacing className={classes.bottonHeader}>
        <CardContent className={classes.caloriesMeal}>
          {Math.ceil(
            meal.foodsId.reduce(
              (acc: number, foodId: number) =>
                acc + foodsList[foodId - 1]?.attributes.energy.kcal,
              0
            )
          )}{" "}
          kcal
        </CardContent>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          {meal.foodsId.length === 0 ? (
            <div className={classes.imgContainer}>
              <CardMedia
                className={classes.cardMedia}
                component="img"
                src={NoFood}
                alt="Food image"
              />
              <span className={classes.cardMediaSpan}>
                Nenhum alimento adicionado
              </span>
            </div>
          ) : (
            <div>
              <ContainerCards>
                {meal.foodsId.map((id: number, idx: number) => {
                  const food = foodsList[id - 1];
                  return (
                    <li key={idx} onClick={() => CallButton(idx)}>
                      <div>
                        {food?.description}
                        <p>Qtd: {food?.base_qty + food?.base_unit}</p>
                        <p>Kcal:{Math.ceil(food?.attributes.energy.kcal)}</p>
                      </div>
                      <div>
                        {IdButton === idx && buttonDelet && (
                          <button onClick={() => remover(id, meal.id, idx)}>
                            -
                          </button>
                        )}
                      </div>
                    </li>
                  );
                })}
              </ContainerCards>
            </div>
          )}
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default DietCard;
