import styled from "styled-components";

export const ContainerCards = styled.ul`
  li {
    cursor: pointer;
    margin-bottom: 10px;
    display: flex;
    width: 100%;
    align-items: center;
  }
  p {
    color: #a1a1a1;
    font-size: 14px;
  }
  button {
    color: white;
    font-size: 25px;
    width: 25px;
    height: 25px;
    line-height: 25px;
    border-radius: 50%;
    border: none;
    background-color: #a52451;
    margin-left: 20px;

    &:active {
      transform: translateY(2px);
    }

    span {
      font-size: 25px;
      color: #ffffff;
    }
  }
`;
