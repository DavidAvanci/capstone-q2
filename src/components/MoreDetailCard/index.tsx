import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Container } from "./style";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
      minHeight: 15,
      border: "0px 0px 1px 0px solid black",
      marginBottom: "15px",
      margin: "0 auto",
      borderBottom: "4px solid var(--color-terciary)",
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    moreDetails: {
      color: "#000000",
      fontWeight: "bold",
      fontSize: "18px",
      padding: "5px",
    },
    bottonHeader: {
      padding: "0px",
      height: "30px",
    },
  })
);

interface IMoreDetail {
  fiber: number;
  saturated: number;
  monounsaturated: number;
  polyunsaturated: number;
  sodium: number;
  potassium: number;
  cholesterol: number;
}

const MoreDetailCard = ({
  fiber,
  saturated,
  monounsaturated,
  polyunsaturated,
  sodium,
  potassium,
  cholesterol,
}: IMoreDetail) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardActions disableSpacing className={classes.bottonHeader}>
        <CardContent className={classes.moreDetails}>Mais detalhes</CardContent>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Container>
            <h3>Carboidratos</h3>
            <div className="nutrients">
              <span>Fibras</span>
              <span className="grams">{fiber}g</span>
            </div>
          </Container>
          <Container>
            <h3>Gorduras</h3>
            <div className="nutrients">
              <span>Gorduras Saturadas</span>
              <span className="grams">{saturated}g</span>
            </div>
            <div className="nutrients">
              <span>Gorduras Poliinsaturadas</span>
              <span className="grams">{polyunsaturated}g</span>
            </div>
            <div className="nutrients">
              <span>Gorduras Monoinsaturadas</span>
              <span className="grams">{monounsaturated}g</span>
            </div>
          </Container>

          <Container>
            <h3>Outros</h3>
            <div className="nutrients">
              <span>Sódio</span>
              <span className="grams">{sodium}mg</span>
            </div>
            <div className="nutrients">
              <span>Colesterol</span>
              <span className="grams">{cholesterol}mg</span>
            </div>
            <div className="nutrients">
              <span>Potássio</span>
              <span className="grams">{potassium}mg</span>
            </div>
          </Container>
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default MoreDetailCard;
