import { useHistory } from "react-router-dom";
import { Container, Voltar } from "./styles";

interface BackProps {
  irPara: string;
  NomePage: string;
}

export const Back = ({ irPara, NomePage }: BackProps) => {
  const history = useHistory();
  return (
    <>
      <Voltar>
        <div onClick={() => history.push(irPara)}>
          <span>&#60;</span>{" "}
        </div>
      </Voltar>
      <Container>
        <h5>{NomePage}</h5>
      </Container>
    </>
  );
};
