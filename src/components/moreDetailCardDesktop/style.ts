import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px;

  h3 {
    font-size: 30px;
  }

  .nutrients {
    display: flex;
    justify-content: start;
    border-bottom: 1px solid black;
    margin-bottom: 7px;
    font-size: 16px;
  }

  .grams {
    margin-left: auto;
    font-size: 16px;
  }

  .title {
    text-align: center;
    font-size: 20px;
    margin-bottom: 5px;
  }
`;
