import { useState } from "react";
import { useParams } from "react-router";
import { useFoodsContext } from "../../providers/Foods";
import { useUserDietContext } from "../../providers/UserDiet";
import { Back } from "../../components/Back";
import {
  AddButton,
  BackAndTitle,
  Details,
  FoodCard,
  SearchContainer,
  ParentContainer,
} from "./style";
import { FaSearch } from "react-icons/fa";
import { Header } from "../../components/Header";

interface IParams {
  mealId: string;
}

export const AddFoods = () => {
  const { foodsList, categoryList } = useFoodsContext();
  const { addFoodToDiet } = useUserDietContext();
  const [inputSearch, setInputSearch] = useState<string>("");
  const [filteredList, setFilteredList] = useState(foodsList);
  const { mealId } = useParams<IParams>();
  const mealIdNumber = parseInt(mealId);

  const handleClick = () => {
    const newList = foodsList.filter((food) =>
      food.description.toLowerCase().includes(inputSearch.toLowerCase())
    );
    setFilteredList(newList);
    setInputSearch("");
  };

  return (
    <ParentContainer>
      <Header />
      <BackAndTitle>
        <Back irPara="/dietPlan/personal" NomePage="" />
        <p>Buscar - Comida</p>
      </BackAndTitle>
      <SearchContainer>
        <input
          value={inputSearch}
          onChange={(e) => setInputSearch(e.target.value)}
        />
        <div>
          <FaSearch onClick={() => handleClick()} />
        </div>
      </SearchContainer>
      <ul>
        {filteredList.map((food, idx) => {
          const category = categoryList.find(
            (elem) => elem.id === food.category_id
          );
          const calories = Math.round(food.attributes.energy.kcal);
          return (
            <FoodCard key={idx}>
              <h2>{food.description}</h2>
              <Details>
                <div>
                  <p>{category?.category}</p>
                  <p>
                    <span className="calories">{calories}Kcal</span> /{" "}
                    {food.base_qty + food.base_unit}
                  </p>
                </div>
                <AddButton onClick={() => addFoodToDiet(food.id, mealIdNumber)}>
                  +
                </AddButton>
              </Details>
            </FoodCard>
          );
        })}
      </ul>
    </ParentContainer>
  );
};
