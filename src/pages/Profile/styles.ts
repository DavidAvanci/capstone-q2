import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: start;
  flex-direction: column;
  height: calc(100vh - 180px);

  h3 {
    margin-top: 10px;
  }
  h4 {
    color: #a1a1a1;
    margin-top: 10px;
    margin-bottom: 15px;
  }
  .profileImg {
    float: left;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 18vh;
    height: 16vh;
    border-radius: 100%;
    background: var(--color-primary);
    box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.66);
  }
  .letter {
    font-size: 72px;
    font-weight: bolder;
    color: var(--color-base-default);
    line-height: 100px;
  }
  span {
    position: absolute;
    margin: 75%;
  }
  .icone {
    background-color: #ffc0b8;
    width: 5vh;
    height: 5vh;
    color: #a52451;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 4vw;
    box-shadow: 0px 4px 4px 0px #00000040;
    svg {
      width: 3.5vh;
      height: 3.5vh;
    }
  }
  div {
    display: flex;
    width: 80%;
    height: 6.6vh;
    align-items: center;
    cursor: pointer;
  }

  @media (min-width: 768px) {
    width: 40%;
    margin: 0 auto;
    height: calc(100vh - 130px);

    span {
      color: transparent;
      margin: 0;
      transition: all 0.3s;
    }

    .icone {
      transition: all 0.3s;
    }

    .options:hover span {
      display: block;
      margin: 0 0 0 30%;
      color: black;
    }

    .options:hover .icone {
      width: 6vh;
      height: 6vh;
    }
  }
`;
