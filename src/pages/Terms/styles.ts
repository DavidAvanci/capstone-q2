import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: 16px;
  margin-right: 16px;

  @media (min-width: 768px){
    margin-left: 48px;
    margin-right: 48px;
  }
  img {
    width: 120px;
    height: 70px;
    margin-bottom: 32px;
    @media (min-width: 480px){
      width: 240px;
      height: auto;
    }
    @media (min-width: 768px){
      width: 320px;
      height: auto;
    }
  }

  div.terms--terms{
    background-color: var(--color-primary-two);
    padding: 16px;
    border-radius: 16px;
    margin-bottom: 48px;
  }

  div.terms--politics{
    background-color: var(--color-primary-two);
    padding: 16px;
    border-radius: 16px;
    margin-bottom: 48px;
  }

  h3 {
    margin-top: 16px;
    margin-bottom: 16px;
    color: var(--color-title);
    font-family: 'Signika', sans-serif;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 32px;
    letter-spacing: 0.05em;
    text-align: center;
    @media (min-width: 768px) {
      font-size: 32px;
    }
  }
  p {
    font-family: "Roboto Condensed", sans-serif;
    color: var(--color-placeholder);
    text-align: justify;
    @media (min-width: 768px) {
      font-size: 20px;
    }
  }
  a {
    color: var(--color-terciary);
    cursor: pointer;
  }
`;
