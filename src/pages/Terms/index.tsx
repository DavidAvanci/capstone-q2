import { useState } from "react";
import { Back } from "../../components/Back";
import { Container } from "./styles";
import logo from "../../assets/logo-secondarySelf.png";
import { useHistory } from "react-router-dom";
import Button from "../../components/Button";
import { Header } from "../../components/Header";

export const Terms = () => {
  const [lerMaisTerms, setLerMaisTerms] = useState<boolean>(false);
  const [lerMaisPrivacy, setLerMaisPrivacy] = useState<boolean>(false);
  const history = useHistory();
  return (
    <>
    <Header />
      <Back irPara="/profile" NomePage="Termos e Políticas" />
      <Container>
        <img src={logo} alt="logo"></img>
        <div className="terms--terms">
          <h3>Termos de Uso</h3>
          <p>
            1. Termos Ao acessar ao site Nutriself, concorda em cumprir estes
            termos de serviço, todas as leis e regulamentos aplicáveis ​​e
            concorda que é responsável pelo cumprimento de todas as leis locais
            aplicáveis. Se você não concordar com algum desses termos, está
            proibido de usar ou acessar este site.
            {lerMaisTerms ? (
              <span className="tudo">
                {" "}
                Os materiais contidos neste site são protegidos pelas leis de
                direitos autorais e marcas comerciais aplicáveis. 2. Uso de
                Licença É concedida permissão para baixar temporariamente uma
                cópia dos materiais (informações ou software) no site Nutriself
                , apenas para visualização transitória pessoal e não comercial.
                Esta é a concessão de uma licença, não uma transferência de
                título e, sob esta licença, você não pode: modificar ou copiar
                os materiais; usar os materiais para qualquer finalidade
                comercial ou para exibição pública (comercial ou não comercial);
                tentar descompilar ou fazer engenharia reversa de qualquer
                software contido no site Nutriself; remover quaisquer direitos
                autorais ou outras notações de propriedade dos materiais; ou
                transferir os materiais para outra pessoa ou 'espelhe' os
                materiais em qualquer outro servidor. Esta licença será
                automaticamente rescindida se você violar alguma dessas
                restrições e poderá ser rescindida por Nutriself a qualquer
                momento. Ao encerrar a visualização desses materiais ou após o
                término desta licença, você deve apagar todos os materiais
                baixados em sua posse, seja em formato eletrónico ou impresso.
                3. Isenção de responsabilidade Os materiais no site da Nutriself
                são fornecidos 'como estão'. Nutriself não oferece garantias,
                expressas ou implícitas, e, por este meio, isenta e nega todas
                as outras garantias, incluindo, sem limitação, garantias
                implícitas ou condições de comercialização, adequação a um fim
                específico ou não violação de propriedade intelectual ou outra
                violação de direitos. Além disso, o Nutriself não garante ou faz
                qualquer representação relativa à precisão, aos resultados
                prováveis ​​ou à confiabilidade do uso dos materiais em seu site
                ou de outra forma relacionado a esses materiais ou em sites
                vinculados a este site. 4. Limitações Em nenhum caso o Nutriself
                ou seus fornecedores serão responsáveis ​​por quaisquer danos
                (incluindo, sem limitação, danos por perda de dados ou lucro ou
                devido a interrupção dos negócios) decorrentes do uso ou da
                incapacidade de usar os materiais em Nutriself, mesmo que
                Nutriself ou um representante autorizado da Nutriself tenha sido
                notificado oralmente ou por escrito da possibilidade de tais
                danos. Como algumas jurisdições não permitem limitações em
                garantias implícitas, ou limitações de responsabilidade por
                danos conseqüentes ou incidentais, essas limitações podem não se
                aplicar a você. 5. Precisão dos materiais Os materiais exibidos
                no site da Nutriself podem incluir erros técnicos, tipográficos
                ou fotográficos. Nutriself não garante que qualquer material em
                seu site seja preciso, completo ou atual. Nutriself pode fazer
                alterações nos materiais contidos em seu site a qualquer
                momento, sem aviso prévio. No entanto, Nutriself não se
                compromete a atualizar os materiais. 6. Links O Nutriself não
                analisou todos os sites vinculados ao seu site e não é
                responsável pelo conteúdo de nenhum site vinculado. A inclusão
                de qualquer link não implica endosso por Nutriself do site. O
                uso de qualquer site vinculado é por conta e risco do usuário.
                Modificações O Nutriself pode revisar estes termos de serviço do
                site a qualquer momento, sem aviso prévio. Ao usar este site,
                você concorda em ficar vinculado à versão atual desses termos de
                serviço. Lei aplicável Estes termos e condições são regidos e
                interpretados de acordo com as leis do Nutriself e você se
                submete irrevogavelmente à jurisdição exclusiva dos tribunais
                naquele estado ou localidade.
              </span>
            ) : (
              <span className="ler mais">...</span>
            )}
            {lerMaisTerms ? (
              <a onClick={() => setLerMaisTerms(!lerMaisTerms)}>Ler Menos</a>
            ) : (
              <a onClick={() => setLerMaisTerms(!lerMaisTerms)}>Ler Mais</a>
            )}
          </p>
        </div>
        <div className="terms--politics">
          <h3>Política de Privacidade</h3>
          <p>
            Política de Privacidade A sua privacidade é importante para nós. É
            política do Nutriself respeitar a sua privacidade em relação a
            qualquer informação sua que possamos coletar no site Nutriself, e
            outros sites que possuímos e operamos.
            {lerMaisPrivacy ? (
              <span className="tudo">
                Solicitamos informações pessoais apenas quando realmente
                precisamos delas para lhe fornecer um serviço. Fazemo-lo por
                meios justos e legais, com o seu conhecimento e consentimento.
                Também informamos por que estamos coletando e como será usado.
                Apenas retemos as informações coletadas pelo tempo necessário
                para fornecer o serviço solicitado. Quando armazenamos dados,
                protegemos dentro de meios comercialmente aceitáveis ​​para
                evitar perdas e roubos, bem como acesso, divulgação, cópia, uso
                ou modificação não autorizados. Não compartilhamos informações
                de identificação pessoal publicamente ou com terceiros, exceto
                quando exigido por lei. O nosso site pode ter links para sites
                externos que não são operados por nós. Esteja ciente de que não
                temos controle sobre o conteúdo e práticas desses sites e não
                podemos aceitar responsabilidade por suas respectivas políticas
                de privacidade. Você é livre para recusar a nossa solicitação de
                informações pessoais, entendendo que talvez não possamos
                fornecer alguns dos serviços desejados. O uso continuado de
                nosso site será considerado como aceitação de nossas práticas em
                torno de privacidade e informações pessoais. Se você tiver
                alguma dúvida sobre como lidamos com dados do usuário e
                informações pessoais, entre em contacto connosco. Compromisso do
                Usuário O usuário se compromete a fazer uso adequado dos
                conteúdos e da informação que o Nutriself oferece no site e com
                caráter enunciativo, mas não limitativo: A) Não se envolver em
                atividades que sejam ilegais ou contrárias à boa fé a à ordem
                pública; B) Não difundir propaganda ou conteúdo de natureza
                racista, xenofóbica, ou apostas online (ex.: Moosh), jogos de
                sorte e azar, qualquer tipo de pornografia ilegal, de apologia
                ao terrorismo ou contra os direitos humanos; C) Não causar danos
                aos sistemas físicos (hardwares) e lógicos (softwares) do
                Nutriself, de seus fornecedores ou terceiros, para introduzir ou
                disseminar vírus informáticos ou quaisquer outros sistemas de
                hardware ou software que sejam capazes de causar danos
                anteriormente mencionados. Mais informações Esperemos que esteja
                esclarecido e, como mencionado anteriormente, se houver algo que
                você não tem certeza se precisa ou não, geralmente é mais seguro
                deixar os cookies ativados, caso interaja com um dos recursos
                que você usa em nosso site. Esta política é efetiva a partir de
                September/2021.{" "}
              </span>
            ) : (
              <span className="ler mais">...</span>
            )}
            {lerMaisPrivacy ? (
              <a onClick={() => setLerMaisPrivacy(!lerMaisPrivacy)}>
                Ler Menos
              </a>
            ) : (
              <a onClick={() => setLerMaisPrivacy(!lerMaisPrivacy)}>Ler Mais</a>
            )}
          </p>
        </div>
        <div>
          <Button onClick={() => history.push("/profile")}>
            Voltar ao Perfil
          </Button>
        </div>
      </Container>
    </>
  );
};
