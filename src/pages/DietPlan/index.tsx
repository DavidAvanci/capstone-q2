import { useEffect, useState } from "react";
import CircularProgessBarComp from "../../components/ProgressBar";
import DietCard from "../../components/DietCard";
import { Navbar } from "../../components/NavBar";
import { useUserDietContext } from "../../providers/UserDiet";
import { Header } from "../../components/Header";
import MiniProgessBar from "../../components/MiniProgessBar";
import MoreDetailCard from "../../components/MoreDetailCard";
import { Choise } from "./styles";
import logoClient from "../../assets/Logo8.png";
import logoNutri from "../../assets/Logo7.png";
import toast from "react-hot-toast";
import { useFoodsContext } from "../../providers/Foods";
import { Container } from "./styles";
import { useParams } from "react-router";
import MiniBarDestop from "../../components/MiniBarDesktop";
import MoreDetailCardDesktop from "../../components/moreDetailCardDesktop";
import { Back } from "../../components/Back";

interface IAtributes {
  energy: ISeila;
  protein: ISeila;
  lipid: ISeila;
  carbohydrate: ISeila;
  cholesterol: ISeila;
  potassium: ISeila;
  sodium: ISeila;
  fiber: ISeila;
}
interface IFat {
  saturated: IMacro;
  monounsaturated: IMacro;
  polyunsaturated: IMacro;
}
interface ISeila {
  qty: number;
  unit: number;
  kcal: number;
  kj: number;
}
interface IMacro {
  qty: number;
  unit: number;
}
interface IParams {
  pageType: string;
}
export const DietPlan = () => {
  const { dietPlan, userTMB, macros, getDiet, basalMetabolicRate } =
    useUserDietContext();
  const { foodsList } = useFoodsContext();
  const { pageType } = useParams<IParams>();
  const [selected, setSelected] = useState<string>(pageType);
  useEffect(() => {
    getDiet();
    basalMetabolicRate();
  }, []);
  //calories
  const totalCalories = () => {
    const array = dietPlan.map((meal) =>
      meal.foodsId.reduce((acc: number, elem: number, idx: number) => {
        const food = foodsList[elem - 1];
        return (
          acc +
          (!isNaN(food?.attributes.energy.kcal)
            ? food?.attributes.energy.kcal
            : 0)
        );
      }, 0)
    );
    return Math.round(array.reduce((acc: number, elem: number) => acc + elem));
  };
  // nutrients
  // const nutrientsCalc = (macro: keyof IAtributes) => {
  //   const array = dietPlan.map((meal) =>
  //     meal.foodsId.reduce((acc: number, elem: number, idx: number) => {
  //       const food = foodsList[elem - 1];
  //       return (
  //         acc +
  //         (!isNaN(food?.attributes[macro].qty)
  //           ? food?.attributes[macro].qty
  //           : 0)
  //       );
  //     }, 0)
  //   ).reduce((acc: number, elem: number) => acc + elem)
  // };
  // //fat
  // const fatCalc = (macro: keyof IFat) => {
  //   const array = dietPlan.map((meal) =>
  //     meal.foodsId.reduce((acc: number, elem: number, idx: number) => {
  //       const food = foodsList[elem - 1];
  //       return (
  //         acc +
  //         (!isNaN(food?.attributes.fatty_acids[macro].qty)
  //           ? food?.attributes.fatty_acids[macro].qty
  //           : 0)
  //       );
  //     }, 0)
  //   );
  //   return Math.round(array.reduce((acc: number, elem: number) => acc + elem));
  // };
  return (
    <div>
      <Header />
      <Back irPara={"/profile"} NomePage={"Planos de Dieta - Escolha"} />
      <Container>
        {selected === "choose" ? (
          <Choise>
            <div
              className="escolha available"
              onClick={() => setSelected("personal")}
            >
              <p>
                Plano de dieta <br></br> Pessoal
              </p>
              <img src={logoClient} alt="logo cliente"></img>
            </div>
            <div
              className="escolha"
              onClick={() => toast.error("Em manutenção !")}
            >
              <p style={{color:"gray"}}>
                Plano de dieta <br></br> Profissional
              </p>
              <img src={logoNutri} alt="logo nutricionista"></img>
            </div>
            <p className="warning--nutri">Em construção</p>
          </Choise>
        ) : selected === "personal" ? (
          <div className="contentContainer">
            <div className="circular__mini">
              <CircularProgessBarComp
                maxValue={userTMB}
                score={userTMB - (userTMB - totalCalories())}
              />
              <div className="miniProgressBar__mobile">
                <MiniProgessBar
                  foodGrans={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.carbohydrate?.qty)
                                ? food?.attributes.carbohydrate?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  macro="Carboidrato"
                  totalGrans={macros.carbs}
                />
                <MiniProgessBar
                  foodGrans={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.protein?.qty)
                                ? food?.attributes.protein?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  macro="Proteína"
                  totalGrans={macros.protein}
                />
                <MiniProgessBar
                  foodGrans={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.lipid?.qty)
                                ? food?.attributes.lipid?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  macro="Gordura"
                  totalGrans={macros.fat}
                />
              </div>
              <div className="miniDesktop">
                <MiniBarDestop
                  foodGrans={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.carbohydrate?.qty)
                                ? food?.attributes.carbohydrate?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  macro="Carboidrato"
                  totalGrans={macros.carbs}
                />
                <MiniBarDestop
                  foodGrans={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.protein?.qty)
                                ? food?.attributes.protein?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  macro="Proteína"
                  totalGrans={macros.protein}
                />
                <MiniBarDestop
                  foodGrans={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.lipid?.qty)
                                ? food?.attributes.lipid?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  macro="Gordura"
                  totalGrans={macros.fat}
                />
              </div>
            </div>
            <div className="more__list">
              <div className="moreDetail">
                <MoreDetailCard
                  fiber={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.fiber?.qty)
                                ? food?.attributes.fiber?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  saturated={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(
                                food?.attributes.fatty_acids?.saturated?.qty
                              )
                                ? food?.attributes.fatty_acids?.saturated?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  monounsaturated={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(
                                food?.attributes.fatty_acids?.monounsaturated
                                  ?.qty
                              )
                                ? food?.attributes.fatty_acids?.monounsaturated
                                    .qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  polyunsaturated={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(
                                food?.attributes.fatty_acids?.polyunsaturated
                                  ?.qty
                              )
                                ? food?.attributes.lipid?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  sodium={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.sodium?.qty)
                                ? food?.attributes.sodium?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  potassium={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.potassium?.qty)
                                ? food?.attributes.potassium?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  cholesterol={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.cholesterol?.qty)
                                ? food?.attributes.cholesterol?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                />
              </div>

              <div className="list">
                {dietPlan.map((meal, mealId) => {
                  return <DietCard key={mealId} meal={meal} mealId={meal.id} />;
                })}
              </div>
              <div className="moreDetailDesktop">
                <MoreDetailCardDesktop
                  fiber={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.fiber?.qty)
                                ? food?.attributes.fiber?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  saturated={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(
                                food?.attributes.fatty_acids?.saturated?.qty
                              )
                                ? food?.attributes.fatty_acids?.saturated?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  monounsaturated={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(
                                food?.attributes.fatty_acids?.monounsaturated
                                  ?.qty
                              )
                                ? food?.attributes.fatty_acids?.monounsaturated
                                    .qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  polyunsaturated={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(
                                food?.attributes.fatty_acids?.polyunsaturated
                                  ?.qty
                              )
                                ? food?.attributes.lipid?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  sodium={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.sodium?.qty)
                                ? food?.attributes.sodium?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  potassium={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.potassium?.qty)
                                ? food?.attributes.potassium?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                  cholesterol={Math.round(
                    dietPlan
                      .map((meal) =>
                        meal.foodsId.reduce(
                          (acc: number, elem: number, idx: number) => {
                            const food = foodsList[elem - 1];
                            return (
                              acc +
                              (!isNaN(food?.attributes.cholesterol?.qty)
                                ? food?.attributes.cholesterol?.qty
                                : 0)
                            );
                          },
                          0
                        )
                      )
                      .reduce((acc: number, elem: number) => acc + elem)
                  )}
                />
              </div>
            </div>
          </div>
        ) : (
          <div>Professional</div>
        )}
      </Container>
      <Navbar />
    </div>
  );
};
