import styled from "styled-components";
export const ContainerPreLogin = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  min-height: 100vh;
`;
export const LeftContainerDesktop = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0 16px;
  }
  img {
    display: none;
    @media (min-width: 1100px) {
      width: 320px;
      display: block;
    }
  }
`;
export const CenterContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: center;
  justify-content: center;
  @media (min-width: 768px) {
    margin: 0 16px;
    /* width:33vw; */
  }
  img.Logo--Desktop__prelogin {
    width: 220px;
    padding-top: 16px;
    @media (min-width: 768px) {
      display: none;
    }
  }
  button.start--prelogin {
    @media (min-width: 768px) {
      display: none;
    }
  }
  p.desc--Desktop__prelogin {
    font-size: 16px;
    color: var(--color-text);
    font-family: "Signika", sans-serif;
    text-align: center;
    @media (min-width: 768px) {
      display: none;
    }
    a {
      font-size: 24px;
      color: var(--color-primary);
      font-family: "Signika", sans-serif;
      text-decoration: none;
      @media (min-width: 768px) {
        display: none;
      }
      &:hover {
        text-decoration: none;
        color: var(--color-terciary);
      }
    }
  }
  button.About_us {
    width: 160px;
    margin-top: 48px;
    @media (min-width: 768px) {
      width: 160px;
      margin-top: 48px;
    }
  }
`;
export const RightContainerDesktop = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    align-content: center;
    justify-content: space-evenly;
    margin: 0 16px;
    min-height: 100vh;
  }
  @media (min-width: 1100px) {
    justify-content: center;
  }
  img {
    display: none;
    @media (min-width: 768px) {
      display: block;
      width: 320px;
    }
    @media (min-width: 1100px) {
      display: none;
    }
  }
  button.start-prelogin {
    display: none;
    @media (min-width: 768px) {
      display: block;
      width: 312px;
      height: 72px;
      font-size: 32px;
    }
  }
  p {
    display: none;
    @media (min-width: 768px) {
      display: flex;
      font-size: 24px;
      color: var(--color-text);
      font-family: "Signika", sans-serif;
      text-align: center;
    }
  }
  a {
    display: none;
    @media (min-width: 768px) {
      display: contents;
      font-size: 40px;
      color: var(--color-primary);
      font-family: "Signika", sans-serif;
      text-decoration: none;
      &:hover {
        text-decoration: none;
        color: var(--color-terciary);
      }
    }
  }
`;
