import styled from "styled-components";

export const Container = styled.div`
  min-height: calc(100vh - 130px);
`;

export const ContainerOptions = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: calc(100vh - 180px);
  justify-content: space-evenly;

  button {
    width: 280px;
    border: 2px solid var(--color-primary-two);
    background-color: var(--color-primary-two);
    height: 160px;
    border-radius: 16px;

    @media (min-width: 768px) {
      width: 400px;
      height: 200px;
    }
    @media (min-width: 1100px) {
      width: 480px;
      height: 240px;
    }
    &:active {
      border: 2px solid var(--color-terciary);
      background-color: var(--color-terciary-two);
    }
  }

  div.card--warning__nutri {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    p.warning--nutri {
      color: var(--color-terciary);
      margin-top: 8px;
      font-size: 16px;
      @media (min-width: 768px) {
        font-size: 20px;
      }
      @media (min-width: 1100px) {
        font-size: 24px;
      }
    }
  }

  div.card--search__food {
    font-family: "Signika", sans-serif;
    text-transform: none;
    font-weight: 300;
    color: var(--color-text);
    text-align: left;
    font-size: 18px;
    line-height: 1.1;
    padding: 8px 4px;
    height: 140px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: left;

    @media (min-width: 768px) {
      font-size: 24px;
      height: 180px;
      padding: 12px 8px;
    }
    @media (min-width: 1100px) {
      font-size: 30px;
      height: 220px;
      padding: 16px 12px;
    }
  }

  div.card--search__nutri {
    font-family: "Signika", sans-serif;
    text-transform: none;
    font-weight: 300;
    color: var(--color-placeholder);
    text-align: left;
    font-size: 18px;
    line-height: 1.1;
    padding: 8px 4px;
    height: 140px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: left;
    @media (min-width: 768px) {
      font-size: 24px;
      height: 180px;
      padding: 12px 8px;
    }
    @media (min-width: 1100px) {
      font-size: 30px;
      height: 220px;
      padding: 16px 12px;
    }
  }

  h4 {
    font-family: "Signika", sans-serif;
    font-weight: 400;
    font-size: 28px;
    @media (min-width: 768px) {
      font-size: 32px;
    }
    @media (min-width: 1100px) {
      font-size: 36px;
    }
  }
  img {
    width: 96px;
    @media (min-width: 768px) {
      width: 144px;
    }
    @media (min-width: 1100px) {
      width: 192px;
    }
  }
  span {
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    width: 300px;
    @media (min-width: 768px) {
      width: 420px;
    }
    @media (min-width: 1100px) {
      width: 500px;
    }
  }
`;

export const BackAndTitle = styled.div`
  text-align: center;
  margin: 10px 0;
  div {
    width: fit-content;
    float: left;
  }
  p {
    margin: 0 auto;
  }
`;

export const SearchContainer = styled.div`
  width: 90%;
  margin: 15px auto;
  display: flex;
  justify-content: space-evenly;
  background-color: #f4f4f4;
  padding: 10px 25px;
  border-radius: 15px;
  @media (min-width: 768px) {
    width: 500px;
  }
  @media (min-width: 1100px) {
    width: 720px;
  }

  input {
    width: 232px;
    /* border: none; */
    background-color: transparent;
    height: 32px;
    border-radius: 16px;
    border: 1px solid var(--color-primary);
    @media (min-width: 768px) {
      width: 320px;
    }
    @media (min-width: 1100px) {
      width: 480px;
    }
  }

  div.search--bar {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  div {
    background-color: var(--color-primary);
    width: 72px;

    color: var(--color-base-default);
    cursor: pointer;
    border-radius: 48px;
    text-align: center;
    @media (min-width: 768px) {
      width: 96px;
    }
    @media (min-width: 1100px) {
      width: 122px;
    }
  }
`;

export const FoodCard = styled.div`
  background-color: #eff7ee;
  width: 90%;
  padding: 10px;
  border-radius: 15px;
  margin: 5px auto;
  box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);

  h2 {
    font-size: 25px;
  }

  .calories {
    color: #6cb663;
  }
`;

export const Details = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  img {
    width: 50px;
  }
`;
