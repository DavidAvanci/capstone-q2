import React, { useState } from "react";
import Foods from "../../assets/genericFood.png";
import Nutri from "../../assets/nutritionist.png";
import foodCategory1 from "../../assets/foodCategory1.png";
import foodCategory2 from "../../assets/foodCategory2.png";
import foodCategory3 from "../../assets/foodCategory3.png";
import foodCategory4 from "../../assets/foodCategory4.png";
import foodCategory5 from "../../assets/foodCategory5.png";
import foodCategory6 from "../../assets/foodCategory6.png";
import foodCategory7 from "../../assets/foodCategory7.png";
import foodCategory8 from "../../assets/foodCategory8.png";
import foodCategory9 from "../../assets/foodCategory9.png";
import foodCategory10 from "../../assets/foodCategory10.png";
import foodCategory11 from "../../assets/foodCategory11.png";
import foodCategory12 from "../../assets/foodCategory12.png";
import foodCategory13 from "../../assets/foodCategory13.png";
import foodCategory14 from "../../assets/foodCategory14.png";
import foodCategory15 from "../../assets/foodCategory15.png";

import { Button } from "@material-ui/core";
import { Container, ContainerOptions } from "./styles";
import { Navbar } from "../../components/NavBar";
import { Header } from "../../components/Header";

import { BackAndTitle, Details, FoodCard, SearchContainer } from "./styles";
import { FaSearch } from "react-icons/fa";
import { useFoodsContext } from "../../providers/Foods";
import { Back } from "../../components/Back";

export const Search = () => {
  const images = [
    foodCategory1,
    foodCategory2,
    foodCategory3,
    foodCategory4,
    foodCategory5,
    foodCategory6,
    foodCategory7,
    foodCategory8,
    foodCategory9,
    foodCategory10,
    foodCategory11,
    foodCategory12,
    foodCategory13,
    foodCategory14,
    foodCategory15,
  ];
  const { foodsList, categoryList } = useFoodsContext();
  const [filteredList, setFilteredList] = useState(foodsList);
  const [inputSearch, setInputSearch] = useState<string>("");
  const [render, setRender] = useState<string>("");

  const handleClick = (inputSearch: string) => {
    const newList = foodsList.filter((food) =>
      food.description.toLowerCase().includes(inputSearch.toLowerCase())
    );
    setFilteredList(newList);
  };

  return (
    <>
    
      <Container>
        <Header />
        <Back irPara="/profile" NomePage="Buscar" />
        {render === "" ? (
          <ContainerOptions>
            <Button
              onClick={() => {
                setRender("comida");
                setFilteredList(foodsList);
              }}
            >
              <div className="card--search__food">
                <h4>Comida</h4>
                <p>Encontre aqui informações sobre alimentos</p>
              </div>
              <div>
                <div>
                  <img src={Foods} alt="Foods" />
                </div>
              </div>
            </Button>
            <div className="card--warning__nutri">
              <Button disabled>
                <div className="card--search__nutri">
                  <h4>Nutricionista</h4>
                  <p>
                    Encontre profissionais adequados para as tuas necessidades
                  </p>
                </div>
                <div>
                  <div>
                    <img src={Nutri} alt="Nutricionist" />
                  </div>
                </div>
              </Button>
              <p className="warning--nutri">Em construção</p>
            </div>
          </ContainerOptions>
        ) : render === "comida" ? (
          <div>
            <Back irPara="/profile" NomePage="Buscar - Comida" />
            <BackAndTitle>
              {/* <p>Buscar - Comida</p> */}
            </BackAndTitle>
            <SearchContainer>
              <input
                value={inputSearch}
                onChange={(e) => setInputSearch(e.target.value)}
                onKeyUp={(e) => handleClick(inputSearch)}
              />
              <div className="search--bar">
                <FaSearch
                  onClick={() => {
                    handleClick(inputSearch);
                    setInputSearch("");
                  }}
                />
              </div>
            </SearchContainer>
            <ul>
              {filteredList.map((food, idx) => {
                const category = categoryList.find(
                  (elem) => elem.id === food.category_id
                );
                const calories = Math.round(food.attributes.energy.kcal);
                return (
                  <FoodCard key={idx}>
                    <h2>{food.description}</h2>
                    <Details>
                      <div>
                        <img
                          src={images[food.category_id - 1]}
                          alt={`${category}`}
                        />
                      </div>
                      <div>
                        <p>{category?.category}</p>
                        <p>
                          <span className="calories">{calories}Kcal</span> /{" "}
                          {food.base_qty + food.base_unit}
                        </p>
                      </div>
                    </Details>
                  </FoodCard>
                );
              })}
            </ul>
          </div>
        ) : (
          <div>Nutri</div>
        )}
      </Container>
      <Navbar />
    </>
  );
};
