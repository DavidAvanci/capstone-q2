import styled from "styled-components";
import BackgroundLogin from "../../assets/BackgroundLogin.png";

interface ContainerProps {
  backgroundColor: string;
  step: number;
  gender: string;
  objective: string;
  activity: string;
}

interface ButtonProps {
  backgroundColor: string;
  step: number;
}

export const Container = styled.div<ContainerProps>`
  background-color: ${(props) => (props.step !== 4 ? "#91C787" : "white")};
  height: 100vh;

  span {
    color: var(--color-secondary);
  }

  figure {
    text-align: center;
  }

  figure img {
    width: 220px;
    margin-top: 5%;
  }

  .you {
    color: #df1545;
  }

  .title {
    font-size: 20px;
    text-align: center;
    margin-bottom: 30px;
  }

  .header {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .radioInput {
    display: none;
  }

  .radioInput:checked + img {
    outline: 2px solid blue;
  }

  .genderContainer {
    display: flex;
    justify-content: space-around;
    align-items: baseline;
    margin-bottom: 10px;
  }

  .objectiveContainer {
    display: flex;
    justify-content: space-around;
    align-items: baseline;
    label img {
      height: 130px;
    }
  }

  .activityContainer {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  label {
    width: fit-content;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  select {
    width: 150px;
    font-size: 18px;
  }

  .selectContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .selectContainer select {
    position: relative;
    width: 90%;
    margin-left: 5%;
    height: 40px;
    border-radius: 10px;
    border: none;
    text-transform: capitalize;
    color: black;
    background: white;
    text-align: left;
    padding: 0 15px;
    font-size: 16px;
    cursor: pointer;
  }

  .selectContainer h3 {
    color: white;
    font-size: 18px;
    margin-left: 10%;
  }

  .imc {
    display: flex;
    margin-top: 20px;
    p {
      color: white;
      margin-left: 15%;
      margin-top: 5px;
    }
    .calcImc {
      margin-left: 10px;
      background-color: #9e9ea7;
      color: black;
      padding: 5px 50px 5px 10px;
      border-radius: 10px;
    }
    span {
      color: black;
    }
  }

  .buttons {
    display: flex;
    justify-content: space-around;
    margin: 30px auto 0;
    button {
      height: 40px;
      border-radius: 10px;
      border: none;
      text-transform: capitalize;
      color: #8ac83c;
      background: white;
      text-align: left;
      padding: 0 40px;
      font-size: 16px;
      cursor: pointer;
    }
  }

  .step2 {
    img {
      width: 70px;
      height: 80px;
      outline: none;
    }
    label {
      border-radius: 16px;
      background-color: var(--color-base-default);
      width: 95px;
      border: 2px solid transparent;
      transition: all 0.2s;
    }
    //CSS que salva a borda da opcao que foi selecionada
    .man {
      border: 2px solid
        ${(props) =>
          props.gender === "Homem" ? "var(--color-terciary)" : "transparent"};
    }
    .woman {
      border: 2px solid
        ${(props) =>
          props.gender === "Mulher" ? "var(--color-terciary)" : "transparent"};
    }
    .other {
      border: 2px solid
        ${(props) =>
          props.gender === "Outro" ? "var(--color-terciary)" : "transparent"};
    }
    .lose-weight {
      border: 2px solid
        ${(props) =>
          props.objective === "Perder Peso"
            ? "var(--color-terciary)"
            : "transparent"};
    }
    .gain-weight {
      border: 2px solid
        ${(props) =>
          props.objective === "Ganhar Peso"
            ? "var(--color-terciary)"
            : "transparent"};
    }
    .gain-muscle {
      border: 2px solid
        ${(props) =>
          props.objective === "Ganhar Músculos"
            ? "var(--color-terciary)"
            : "transparent"};
    }

    // Fim

    label:hover {
      transform: translateY(-2px);
      box-shadow: 0px 2px 3px grey;
    }
    .radioInput:checked + img {
      outline: none;
    }
  }

  .step2 h3 {
    color: white;
    font-size: 18px;
    margin-left: 10%;
    margin-bottom: 10px;
  }

  .step3 {
    display: flex;
    flex-direction: column;
    img {
      width: 90px;
    }
    .bottom {
      display: flex;
      flex-direction: row;
      margin-top: 10px;
      align-items: baseline;
      width: 100%;
      justify-content: space-around;
    }
    label {
      border-radius: 16px;
      background-color: var(--color-base-default);
      width: 140px;
      border: 2px solid transparent;
      transition: all 0.2s;
    }
    label:hover {
      border: 2px solid var(--color-terciary);
    }
    .radioInput:checked + img {
      outline: none;
    }
    .sedentary {
      border: 2px solid
        ${(props) =>
          props.activity === "Sedentarismo"
            ? "var(--color-terciary)"
            : "transparent"};
    }
    .moderated {
      border: 2px solid
        ${(props) =>
          props.activity === "Moderado"
            ? "var(--color-terciary)"
            : "transparent"};
    }
    .intense {
      border: 2px solid
        ${(props) =>
          props.activity === "Intenso"
            ? "var(--color-terciary)"
            : "transparent"};
    }
    label:hover {
      transform: translateY(-2px);
      box-shadow: 0px 2px 3px grey;
    }
  }

  .step3 h3 {
    color: white;
    font-size: 18px;
    margin-left: 10%;
  }

  .step4 {
    p.desc--Desktop__login {
      font-size: 16px;
      color: var(--color-text);
      font-family: "Signika", sans-serif;
      text-align: center;
      @media (min-width: 480px) {
        font-size: 24px;
      }
      a {
        font-size: 24px;
        color: var(--color-primary);
        font-family: "Signika", sans-serif;
        text-decoration: none;
        @media (min-width: 768px) {
        }
        &:hover {
          text-decoration: none;
          color: var(--color-terciary);
        }
      }
    }
    .button-page4 {
      display: flex;
      justify-content: center;
    }
  }
`;

export const BackgroundL = styled.div`
  display: none;
  @media (min-width: 900px) {
    display: block;
    flex: 1;
    background: url(${BackgroundLogin}) no-repeat center, var(--color-primary);
    background-color: var(--color-primary);
    width: 700px;
  }
`;

export const LoginImg = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0 16px;
  }
  img {
    width: 220px;
  }
`;

export const LoginForm = styled.div`
  @media (min-width: 768px) {
  }
  form {
    text-align: center;
    height: 320px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
  }
`;

export const ContainerRegister = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: center;
  justify-content: space-evenly;
  height: 300px;
  margin: 0 16px;

  div.label--input {
    padding-left: 16px;
    color: var(--color-placeholder);
    text-align: left;
  }
`;

export const InputContainer = styled.div`
  border: 1px solid var(--color-primary);
  border-radius: 16px 4px 16px 4px;
  input {
    border: none;
    height: 32px;
    outline: none;
    border-radius: 16px 4px 16px 4px;
    font-size: 16px;
    flex: 1;
    border: 0;
    color: var(--color-placeholder);
    padding-left: 8px;
  }
  input::placeholder {
    color: var(--color-placeholder);
    font-size: 16px;
  }

  svg {
    margin-right: 1;
  }
`;

export const Button = styled.button<ButtonProps>`
  background: ${(props) => (props.step !== 4 ? "#ffffff" : "#91c787")};
  width: 140px;
  height: 54px;
  color: var(--color-base-default);

  border-radius: 48px;
  border: 2px solid transparent;

  font-size: 24px;
  font-family: "Signika", sans-serif;

  margin: 16px 8px;
  &:hover {
    background: var(--color);
    color: var(--color-primary);
    border: 2px solid var(--color-primary);
  }
  &:active {
    background: var(--color-terciary);
    color: var(--color-terciary-two);
    border: 2px solid var(--color-terciary-two);
  }
`;
