import { useEffect, useState } from "react";
import { Back } from "../../components/Back";
import { Container, ContainerModal } from "./styles";
import { useEditProfileContext } from "../../providers/EditProfile";
import toast from "react-hot-toast";
import { Header } from "../../components/Header";
export const Editprofile = () => {
  const [modal, setModal] = useState(false);
  const [NameModal, setNameModal] = useState("");
  const [patch, setPatch] = useState("");
  const { editarDadoPerfil, dadosDoUsuario, PegarDadosDoUsuario, dados } =
    useEditProfileContext();
  const [valorDoInput, setValorDoInput] = useState<any>();
  const abrirModal = (a: boolean, b: string, c: string, input: any) => {
    setModal(a);
    setPatch(b);
    setNameModal(c);
    setValorDoInput(input);
  };
  const Atualizado = () => {
    if (valorDoInput !== "") {
      editarDadoPerfil(patch, valorDoInput);
      toast.success("Alterado com sucesso");
    }
    if (valorDoInput === "") {
      toast.error("Digite um valor");
    }
    setModal(false);
    PegarDadosDoUsuario();
    setValorDoInput("");
  };
  useEffect(() => {
    PegarDadosDoUsuario();
  }, [modal]);
  return (
    <>
      <Header />
      <Back NomePage="Editar Perfil" irPara="/profile"></Back>
      {modal && (
        <ContainerModal>
          <div className="modal">
            <div className="modal-content">
              <div className="modal-header">
                <span className="close" onClick={() => setModal(false)}>
                  &times;
                </span>
                <h2>{NameModal}</h2>
              </div>
              <div className="modal-body">
                <div className="trocar">
                  {NameModal === "Editar idade" && (
                    <select
                      onChange={(e) =>
                        setValorDoInput(parseInt(e.target.value))
                      }
                      defaultValue={valorDoInput}
                    >
                      {[...Array(85)].map((_, i) => (
                        <option key={i}>{i + 16}</option>
                      ))}
                    </select>
                  )}
                  {NameModal === "Editar nome" && (
                    <input
                      placeholder="Digite seu novo nome"
                      value={valorDoInput}
                      onChange={(e) => setValorDoInput(e.target.value)}
                      defaultValue={valorDoInput}
                    ></input>
                  )}
                  {NameModal === "Editar altura" && (
                    <select
                      onChange={(e) =>
                        setValorDoInput(parseInt(e.target.value))
                      }
                      defaultValue={valorDoInput}
                    >
                      {[...Array(184)].map((_, i) => (
                        <option key={i}>{i + 60}</option>
                      ))}
                    </select>
                  )}
                  {NameModal === "Editar peso" && (
                    <select
                      onChange={(e) =>
                        setValorDoInput(parseInt(e.target.value))
                      }
                      defaultValue={valorDoInput}
                    >
                      {[...Array(221)].map((_, i) => (
                        <option key={i}>{i + 30}</option>
                      ))}
                    </select>
                  )}
                  {NameModal === "Editar gênero" && (
                    <select
                      onChange={(e) => setValorDoInput(e.target.value)}
                      defaultValue={valorDoInput}
                    >
                      <option key="Outros">Outros</option>
                      <option key="Homem">Homem</option>
                      <option key="Mulher">Mulher</option>
                    </select>
                  )}
                  {NameModal === "Editar objetivo" && (
                    <select
                      onChange={(e) => setValorDoInput(e.target.value)}
                      defaultValue={valorDoInput}
                    >
                      <option key="Perder Peso">Perder Peso</option>
                      <option key="Ganhar Peso">Ganhar Peso</option>
                      <option key="Ganhar Músculos">Ganhar Músculos</option>
                    </select>
                  )}
                  {NameModal === "Editar atividade" && (
                    <select
                      onChange={(e) => setValorDoInput(e.target.value)}
                      defaultValue={valorDoInput}
                    >
                      <option key="Sedentarismo">Sedentarismo</option>
                      <option key="Moderado">Moderado</option>
                      <option key="Intenso">Intenso</option>
                    </select>
                  )}
                  <button onClick={() => Atualizado()}>ATUALIZAR</button>
                </div>
              </div>
            </div>
          </div>
        </ContainerModal>
      )}
      <Container>
        <h4>Detalhes pessoais</h4>
        <div
          className="editavel"
          onClick={() => abrirModal(true, "name", "Editar nome", "")}
        >
          <span>Nome do usuário</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.name}</p>}
        </div>
        <div>
          <span>Foto de perfil</span>
          <div className="profileImg">
            <p className="letter">{dados && dadosDoUsuario.name[0]}</p>
          </div>
        </div>
        <div>
          <span>Senha</span>
          <p>********</p>
        </div>
        <br></br>
        <br></br>
        <div
          className="editavel"
          onClick={() => abrirModal(true, "height", "Editar altura", 150)}
        >
          <span>Altura(cm)</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.height}</p>}
        </div>
        <div
          className="editavel"
          onClick={() => abrirModal(true, "weight", "Editar peso", 65)}
        >
          <span>Peso(kg)</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.weight}</p>}
        </div>
        <div
          className="editavel"
          onClick={() => abrirModal(true, "gender", "Editar gênero", "Outros")}
        >
          <span>Gênero</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.gender}</p>}
        </div>
        <div
          className="editavel"
          onClick={() => abrirModal(true, "age", "Editar idade", 25)}
        >
          <span>Idade</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.age}</p>}
        </div>
        <div
          className="editavel"
          onClick={() =>
            abrirModal(true, "activity", "Editar atividade", "Moderado")
          }
        >
          <span>Atividade</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.activity}</p>}
        </div>
        <div
          className="editavel"
          onClick={() =>
            abrirModal(true, "objective", "Editar objetivo", "Perder Peso")
          }
        >
          <span>Objetivo</span>
          {dadosDoUsuario && <p>{dadosDoUsuario.objective}</p>}
        </div>
      </Container>
    </>
  );
};
