import styled from "styled-components";
export const ContainerModal = styled.div`
  input {
    width: 20vh;
    margin-top: 7px;
  }
  select {
    width: 20vh;
    height: 30px;
    margin-top: 7px;
  }
  button {
    border: none;
    width: 20vh;
    max-width: 25vh;
    margin-top: 10px;
    margin-bottom: 10px;
    color: #ffffff;
    font-family: Signika;
    line-height: 28px;
    letter-spacing: 0.05em;
    text-align: center;
    background: #91c788;
    border-radius: 50px;
  }
  .modal {
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    overflow: auto;
    background-color: rgba(0, 0, 0, 0.5);
  }
  .modal-content {
    margin: 10% auto;
    width: 40vh;
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 7px 20px 0 rgba(0, 0, 0, 0.17);
    animation-name: modalopen;
    animation-duration: var(--modal-duration);
  }
  .modal-header h2 {
    margin: 0;
  }
  .modal-header {
    .close {
      color: #ccc;
      float: right;
      font-size: 30px;
      color: #fff;
    }
    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    background: #91c788;
    padding: 8.5px;
    color: #fff;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }
  .modal-body {
    padding: 10px 20px;
    background: #fff;
  }
  .trocar {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
export const Container = styled.div`
  padding-right: 10px;
  padding-left: 10px;
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  .profileImg {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-right: 6px;
    border-radius: 50%;
    height: 25px;
    width: 25px;
    background: #512da8;
  }
  .letter {
    margin-left: 5px;
    font-size: 12px;
    align-items: center;
    color: white;
  }
  h4 {
    margin-top: 20px;
    margin-bottom: 20px;
    font-family: Signika;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: 25px;
    letter-spacing: 0.05em;
    text-align: left;
  }
  .editavel {
    cursor: pointer;
  }
  .editavel:hover {
    background-color: #91c787;
  }
  div {
    display: flex;
    max-width: 50vh;
    width: 100%;
    height: 4vh;
    margin-top: 1px;
    align-items: center;
    justify-content: space-between;
    border: 1px solid #0000004d;
    background-color: #eff7ee;
    p {
      margin-right: 7px;
    }
    span {
      margin-left: 5px;
      font-family: Signika;
      font-size: 17px;
      font-style: normal;
      font-weight: 400;
      line-height: 21px;
      letter-spacing: 0.05em;
      text-align: left;
      color: #707070;
    }
  }
  img {
    margin-top: 4px;
    width: 20px;
    height: 20px;
    border-radius: 50%;
  }
`;
