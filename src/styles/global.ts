import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`

:root{
    --color-primary: #91c787;
    --color-primary-two: #cfe7cb;
    --color-secondary: #8ac83c;
    --color-secondary-two: #8ec601;
    --color-terciary: #a52451;
    --color-terciary-two: #e69ca3;
    --color-quarternary: #f07f35;
    --color-title: #111111;
    --color-base-default: #ffffff;
    --color-placeholder: #9e9ea7;
    --color-text: #666665;
    --color-error: #df1545;
    --color-success: #25d970;
    }
    
    body, div, ul, button, p, img, nav, a, input, h1, h2, h3, h4, h5, h6, select, figure{
        margin: 0;
        padding: 0;
        list-style-type: none;
        box-sizing: border-box;
        text-decoration: none;
        outline: none;
        font-family: 'Signika', sans-serif;
    }
    body{
        background-color: var(--color-base-default);
    }

    h1{
        font-size: 38px;
    }

    h2{
        font-size: 34px;
    }

    h3 {
        font-size: 24px;
    }

    h4 {
        font-size: 20px;
    }

    button{
        cursor: pointer;
    }
    `;

export default GlobalStyle;
