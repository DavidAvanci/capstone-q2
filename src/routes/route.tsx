import { useAuth } from "../providers/Auth";
import { ReactElement } from "react";
import { Redirect, Route } from "react-router-dom";

interface IRouteProps {
  component: React.ComponentType;
  path: string;
  exact?: boolean;
}

export const PrivateRoute = ({
  component: Component,
  ...rest
}: IRouteProps) => {
  const { isLoggedIn } = useAuth();

  return (
    <Route
      {...rest}
      render={() => {
        return isLoggedIn ? (
          <Component />
        ) : (
          <Redirect to={{ pathname: "/login" }} />
        );
      }}
    />
  );
};

export const PublicRoute = ({ component: Component, ...rest }: IRouteProps) => {
  const { isLoggedIn } = useAuth();

  return (
    <Route
      {...rest}
      render={() => {
        return !isLoggedIn ? (
          <Component />
        ) : (
          <Redirect to={{ pathname: "/dietPlan/choose" }} />
        );
      }}
    />
  );
};
