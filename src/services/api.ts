import axios from "axios";

export const api = axios.create({
  baseURL: "https://api-fake-nutriself.herokuapp.com",
});

export const apiFoods = axios.create({
  baseURL: "https://taco-food-api.herokuapp.com/api/v1/food",
});
